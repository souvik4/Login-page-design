var state = false;
function toggle(){
    if(state)
    {
        document.getElementById("loginPassword").setAttribute("type","password");
        document.getElementById("eye").style.color = "#7a797e";
        document.getElementById("mi-2").style.display = "block";
        document.getElementById("mi-1").style.display = "none";
        state = false;
    }
    else
    {
        document.getElementById("loginPassword").setAttribute("type","text");
        document.getElementById("eye").style.color = "#5887ef";
        document.getElementById("mi-2").style.display = "none";
        document.getElementById("mi-1").style.display = "block";
        state = true;
    }
}


